from brain import Brain
import gym

class Walker:

    def __init__(self, brain_size, load_brain, render_best):
        self.brain = Brain(brain_size, load_brain)  # new brain with X instructions
        self.dead = False
        self.reached_goal = False
        self.is_best = False  # true if this dot is the best dot from the previous generation
        self.fitness = 0.0
        self.env = gym.make('BipedalWalker-v3')
        self.render_best = render_best
        # self.pos = copy.copy(self.map.startpoint)

    def update(self):  # moves the dot according to the brains directions
        if (self.dead is True) or (self.reached_goal is True):
            return
        if self.brain.step >= len(self.brain.directions):
            self.dead = True
            return
        observation, reward, done, info = self.env.step(self.brain.get_move())
        self.fitness += reward
        if reward == -100:
            self.dead = True
        elif done is True:
            self.reached_goal = True
            self.fitness += 10000000
        if self.is_best and self.render_best:
            self.env.render()

    """ def get_fitness(self):
         if self.reached_goal:
             # if the dot reached the goal then the fitness is based on the amount of steps it took to get there
             self.fitness = 1 / 16 + 10000.0 / (self.brain.step ** 2)
         else:  # if the dot didn't reach the goal then the fitness is based on how close it is to the goal
             self.fitness = 1 / (self.map.get_closest_distance(self.pos[X], self.pos[Y]) ** 2)
         return self.fitness"""

    def reset_environment(self):
        self.env.reset()

    def get_baby(self):
        baby = Walker(0, False, self.render_best)
        baby.brain = self.brain.clone()  # babies have the same brain as their parents
        self.env.close()
        return baby
