import numpy as np
import random
import logging
import copy
from walker import Walker

MAX_STEPS = 1599  # after 1600 steps the Environment gives us a done anyway.


class Population:

    def __init__(self, size, brain_size, mutation_factor, load_brain, render_best):
        self.size = size
        self.brain_size = brain_size
        self.mutation_factor = mutation_factor
        self.fitness_sum = 0.0
        self.gen = 1
        self.best_walker_index = 0  # index of the best player in self.players
        self.best_walker_fitness = 0.0
        self.max_steps = MAX_STEPS
        self.walkers = []
        self.envs = []
        self.fitnesses = None
        self.best_fitness = -1000
        for i in range(self.size):
            self.walkers.append(Walker(self.brain_size, load_brain, render_best))
        self.reset_environments()
        if load_brain:
            self.mutate_babies()

    def reset_environments(self):
        for walker in self.walkers:
            walker.reset_environment()

    def update(self):
        for walker in self.walkers:
            # if the player has taken more steps than the best player needed to reach the goal, he's dead
            if walker.brain.step >= self.max_steps:
                walker.dead = True
            else:
                walker.update()

    def calculate_fitness_sum(self):
        self.fitness_sum = 0
        self.fitnesses = np.zeros(self.size)
        for i in range(self.size):
            self.fitnesses[i] = self.walkers[i].fitness
        self.fitnesses -= np.min(self.fitnesses)
        self.fitness_sum = np.sum(self.fitnesses)

    def all_players_finished(self):  # returns whether all the players are either dead or have reached the goal
        for walker in self.walkers:
            if walker.dead is False and walker.reached_goal is False:
                return False
        return True

    def natural_selection(self):  # gets the next generation of players
        self.calculate_fitness_sum()
        self.set_best_walker()
        if self.best_walker_fitness > self.best_fitness:
            self.walkers[self.best_walker_index].brain.save()
            self.best_fitness = self.best_walker_fitness
            print("New best walker found")
        # the champion lives on
        new_walkers = [self.walkers[self.best_walker_index].get_baby()]
        new_walkers[0].is_best = True
        for i in range(1, self.size):
            parent = self.select_parent()  # select parent based on fitness
            new_walkers.append(parent.get_baby())  # get baby from them
        self.walkers = copy.copy(new_walkers)
        self.gen += 1

    # chooses a player from the population to return randomly(considering fitness)
    # this function works by randomly choosing a value between 0 and the fitness-sum
    # then go through all the players and add their fitness to a running sum.
    # If that sum is greater than the random value generated, that player is chosen
    # since players with a higher fitness function add more to the running sum they have a higher chance of being chosen
    def select_parent(self):
        arrow = random.uniform(0, self.fitness_sum)
        running_sum = 0.0  # those are the bars we add together
        for i in range(self.size):
            running_sum += self.fitnesses[i]
            if running_sum > arrow:
                return self.walkers[i]
        # should never get to this point
        logging.error("Critical Error in select_parent")
        return None

    def mutate_babies(self):  # mutates all the brains of the babies
        for i in range(1, len(self.walkers)):  # we don't want to mutate the champion's brain
            self.walkers[i].brain.mutate(self.mutation_factor)

    def set_best_walker(self):  # finds the player with the highest fitness and sets it as the best one
        max_index = np.argmax(self.fitnesses)
        self.best_walker_index = max_index
        self.best_walker_fitness = self.walkers[max_index].fitness

        # if this dot reached the goal then reset the minimum number of steps it takes to get to the goal
        if self.walkers[max_index].reached_goal:
            self.max_steps = self.walkers[max_index].brain.step
            logging.info('Found goal?!')
            logging.info("step:", self.max_steps)

    def increase_moves(self, size):  # increase the number of directions for the brain
        if len(self.walkers[0].brain.directions) < self.max_steps:
            for walker in self.walkers:
                walker.brain.increase_moves(size)
