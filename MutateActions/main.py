from population import Population
import matplotlib.pyplot as plt
import pickle
import sys

INCREASE_BY = 5
BRAIN_SIZE = 50
POP_SIZE = 50
MUTATION_FACTOR = 0.2  # 0 <= x <= 1
GAME_CANCELLED = False
LOAD_BRAIN = False
RENDER_BEST = False
TEST_WALKER = True

if TEST_WALKER:
    LOAD_BRAIN = True


if __name__ == '__main__':
    if TEST_WALKER:
        rewards = []
        steps = []
        with open('rewards.p', 'rb') as fp:
            rewards = pickle.load(fp)
        with open('steps.p', 'rb') as fp:
            steps = pickle.load(fp)
        plt.title(f'{POP_SIZE}, {MUTATION_FACTOR}')
        plt.xlabel('Episodes')
        plt.ylabel('Rewards')
        plt.plot(rewards, label='Rewards')
        plt.plot(steps, label='Steps')
        plt.legend(loc='right')
        plt.savefig(f'./models/{POP_SIZE}, {MUTATION_FACTOR}.png')
        plt.show()
        sys.exit(0)

    population = Population(POP_SIZE, BRAIN_SIZE,MUTATION_FACTOR, LOAD_BRAIN, RENDER_BEST)

    rewards = []
    steps = []

    while population.gen < 2000:  # this is our game

        if population.all_players_finished():  # this is our genetic algorithm after one generation of players
            rewards.append(population.walkers[0].fitness)
            steps.append(len(population.walkers[0].brain.directions))
            print(f'Best Fitness: {population.walkers[0].fitness}')
            population.natural_selection()
            population.mutate_babies()
            population.increase_moves(INCREASE_BY)
            population.reset_environments()

            print(f'Gen: {population.gen}')
            print(f'Best Index: {population.best_walker_index}')
            print(f'Best Fitness: {population.fitnesses[population.best_walker_index]}')
            print(f'Max Steps: {population.max_steps}')


            if population.gen % 10 == 0:
                with open("rewards.p", 'wb') as fp:
                    pickle.dump(rewards, fp)
                with open("steps.p", 'wb') as fp:
                    pickle.dump(steps, fp)
        else:
            population.update()
        # time.sleep(0.1)
