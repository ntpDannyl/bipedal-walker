import numpy as np
import random
import copy
import pickle


class Brain:

    def __init__(self, size, load_brain):
        self.directions = []
        self.step = 0
        if load_brain:
            self.load()
        else:
            self.increase_moves(size)

    def get_move(self):
        move = self.directions[self.step]
        self.step += 1
        return move

    # we want different and random movements
    def increase_moves(self, size):
        for i in range(size):
            self.directions.append(np.random.uniform(-1, 1, 4))

    # returns a copy of the given brain
    def clone(self):
        clone = Brain(len(self.directions), False)
        for i in range(len(self.directions)):
            clone.directions[i] = copy.copy(self.directions[i])
        return clone


    def mutate(self, mutation_factor):  # mutates the brain by setting some of the directions to random movements
        for i in range(len(self.directions)):
            if random.random() <= mutation_factor:
                self.directions[i] = np.random.uniform(-1, 1, 4)

    def save(self):
        with open('best_brain', 'wb') as fp:
            pickle.dump(self.directions, fp)

    def load(self):
        with open('best_brain', 'rb') as fp:
            self.directions = pickle.load(fp)

if __name__ == '__main__':  # for debugging
    brain_inst = Brain(100, True)
    print(brain_inst.directions)
    print(len(brain_inst.directions))
