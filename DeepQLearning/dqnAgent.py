import time

import numpy as np
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.optimizers import Adam

from agentMemoryDQN import agentMemory


def qFunctionNN(lr, outputs, inputs):
    QFunction = Sequential()
    QFunction.add(Dense(24, activation='relu', input_dim=inputs))
    QFunction.add(Dense(24, activation='relu'))
    QFunction.add(Dense(outputs, activation='linear'))
    QFunction.compile(optimizer=Adam(lr=lr), loss='mean_squared_error')
    return QFunction


class dqnAgent(object):
    def __init__(self, lr, gamma, actions, vareps, bSize, observations,
                 epsDec=0.0, epsMin=0.01, memSize=10000, name='Alan', bins=7):
        self.actions = actions
        self.gamma = gamma
        self.vareps = vareps
        self.epsDec = epsDec
        self.epsMin = epsMin
        self.bSize = bSize
        self.bins = bins
        self.memory = agentMemory(memSize, [1, 24], [actions])
        self.Q = qFunctionNN(lr, actions, observations)
        self.name = name
        self.steps = 0

    def addMemory(self, state, action, reward, nextState, done):
        self.memory.addMemory(state, action, reward, nextState, done)

    def getAction(self, observation):
        if np.random.random() < self.vareps:
            action = np.random.uniform(-1, 1, 4)
        else:
            action = self.Q.predict(observation)[0]
        action = self.round_bins(action, self.bins)
        return action

    def round_bins(self, x, bins):
        round_fact = (bins - 1) / 2
        return np.around(x * round_fact) / round_fact

    def learn(self):
        start = time.time()
        if self.memory.mCounter > self.bSize:
            state, action, r, nextState, done = self.memory.getBatch(self.bSize)

            for i in range(self.bSize):
                next_action = np.amax(self.Q.predict(nextState[i]))
                target = r[i]
                if not done[i]:
                    target = (1.0 - 0.1) * r[i] + 0.1 * self.gamma * next_action
                y = self.Q.predict(state[i])
                y[0] = target
                self.Q.fit(x=state[i], y=y, verbose=0, epochs=1)

            self.steps += 1
        print("learn time: ", time.time() - start)

    def saveCNNs(self):
        fname = self.name + '.h5'
        self.Q.save('Q' + fname)

    def loadCNNs(self):
        fname = self.name + '.h5'
        self.Q = load_model('Q' + fname)
