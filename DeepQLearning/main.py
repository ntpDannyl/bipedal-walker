import time

import gym
import numpy as np
from tqdm import tqdm

from dqnAgent import dqnAgent

env = gym.make('BipedalWalker-v3')
nmb_of_actions = env.action_space.shape[0]
nmb_of_obs = env.observation_space.shape[0]
marvin = dqnAgent(gamma=0.99, vareps=1.0, lr=0.001,
                  observations=nmb_of_obs, actions=nmb_of_actions, memSize=25000,
                  epsMin=0.05, bSize=16, epsDec=0.999, bins=7)

rewards = []
epsHistory = []
avg_rewards = []
steps = 0
verbose = False
best_total_reward = -1000

progress = tqdm(range(10000), desc='Training', unit=' episode')
for epochs in progress:
    done = False
    observation = env.reset()
    observation = observation.reshape(1, -1)
    totalReward = 0
    ep_rewards = []
    start = time.time()
    while not done:
        steps += 1
        action = marvin.getAction(observation)
        obs, reward, done, info = env.step(action)
        obs = obs.reshape(1, -1)
        totalReward += reward
        marvin.addMemory(observation, action, reward, obs, int(done))
        if verbose: env.render()
        observation = obs

        ep_rewards.append(reward)
        if len(ep_rewards) > 50 and max(ep_rewards[-50:]) <= 0.1:
            break

    if totalReward > best_total_reward:
        marvin.saveCNNs()
        best_total_reward = totalReward
        print("new best walker found")

    marvin.learn()

    marvin.vareps *= marvin.epsDec
    if marvin.vareps < marvin.epsMin:
        marvin.vareps = marvin.epsMin

    rewards.append(totalReward)
    epsHistory.append(marvin.vareps)
    movingAvr = np.mean(rewards[-20:])
    avg_rewards.append(movingAvr)
    msg = ' Training r=' + str(totalReward)
    msg += ' vareps=' + str(round(marvin.vareps, ndigits=2))
    msg += ' avg=' + str(movingAvr)
    progress.set_description(msg)
    if epochs % 10 == 0:
        np.save("eps.npy", np.array(epsHistory))
        np.save("total_rewards.npy", np.array(rewards))
        np.save("avg_rewards.npy", np.array(avg_rewards))
    if movingAvr > 300: break  # solve condition

marvin.vareps = 0
done = False
observation = env.reset()
observation = observation.reshape(1, -1)
totalReward = 0
while not done:
    steps += 1
    action = marvin.getAction(observation)
    obs, reward, done, info = env.step(action)
    obs = obs.reshape(1, -1)
    totalReward += reward
    # marvin.addMemory(observation, action_ind, reward, obs, int(done))
    env.render()
    observation = obs
