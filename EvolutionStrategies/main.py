from population import Population
import time
import matplotlib.pyplot as plt
import pickle
import sys

HIDDEN_LAYER = 12
BIAS = False
POP_SIZE = 50
MUTATION_FACTOR = 0.1  # 0 <= x <= 1
LEARNING_RATE = 0.1   # 0 <= x <= 1
GENS = 2000
MAX_STEPS = 300  # after 1600 steps the Environment gives us a done anyway.
DECAY_ALPHA = True

VERSION = 1
TEST_WALKER = True
LOAD_BRAIN = False
RENDER_BEST = False
if TEST_WALKER:
    LOAD_BRAIN = True

def plot_reward(rewards):
    plt.title(f'{HIDDEN_LAYER}, {VERSION}, {POP_SIZE}, {LEARNING_RATE}')
    plt.xlabel('Episodes/10')
    plt.ylabel('Rewards')
    plt.plot(rewards)
    plt.savefig(f'./models/{HIDDEN_LAYER}_{VERSION}_{POP_SIZE}_{LEARNING_RATE}.png')
    plt.show()
    plt.cla()

if __name__ == '__main__':
    avg_rewards = []
    best_avg_reward = -1000

    try:
        population = Population(POP_SIZE, HIDDEN_LAYER, BIAS, MUTATION_FACTOR, MAX_STEPS, LOAD_BRAIN, VERSION, LEARNING_RATE, RENDER_BEST)

        if TEST_WALKER:
            rewards = []
            # population.walker.plot_input_weights()
            for i in range(10):
                rewards.append(population.walker.get_reward(10000, True))
                print("Reward: ", rewards[-1])
            print("Average Reward: ", sum(rewards) / len(rewards))
            plot_reward(rewards)
            sys.exit(0)

        for gen in range(GENS):  # this is our game
            start_time = time.time()
            print(f'Gen: {gen}')
            population.mutate()
            population.play_episode()
            population.evolve()
            print("Time for Gen: ", time.time() - start_time)
            if gen % 10 == 0:
                avg_reward = population.get_walker_stats()
                if avg_reward > best_avg_reward:
                    population.walker.save()
                    best_avg_reward = avg_reward
                    print("New best walker found")
                avg_rewards.append(avg_reward)
                population.walker.save_mlp_weights(gen)
                with open(f'./models/{HIDDEN_LAYER}_{VERSION}_{POP_SIZE}_{LEARNING_RATE}_AvgRewards', 'wb') as fp:
                    pickle.dump(avg_rewards, fp)
                if gen == 1000 and DECAY_ALPHA:
                    population.lr = 0.05
                    population.mutation_factor = 0.05
                if gen == 5000 and DECAY_ALPHA:
                    population.lr = 0.01
                    population.mutation_factor = 0.01
        
        plot_reward(avg_rewards)
    except KeyboardInterrupt:
        if not TEST_WALKER:
            plot_reward(avg_rewards)

