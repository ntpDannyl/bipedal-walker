import os

from numpy import cos, sin, arctan
from matplotlib import pyplot

vertical_distance_between_layers = 40
horizontal_distance_between_neurons = 4
neuron_radius = 1
default_line_width = 1

class Neuron:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def draw(self):
        circle = pyplot.Circle((self.x, self.y), radius=neuron_radius, fill=False)
        pyplot.gca().add_patch(circle)


class Layer:
    def __init__(self, network, number_of_neurons, weights):
        self.previous_layer = self.get_previous_layer(network)
        self.y = self.calculate_layer_y_position()
        self.neurons = self.init_neurons(number_of_neurons)
        self.weights = weights

    def init_neurons(self, number_of_neurons):
        neurons = []
        x = self.calc_left_margin(number_of_neurons)
        for iteration in range(number_of_neurons):
            neuron = Neuron(x, self.y)
            neurons.append(neuron)
            x += horizontal_distance_between_neurons
        return neurons

    def calc_left_margin(self, number_of_neurons):  # so it's centered
        return -horizontal_distance_between_neurons * number_of_neurons/2

    def calculate_layer_y_position(self):
        if self.previous_layer:
            return self.previous_layer.y + vertical_distance_between_layers
        else:
            return 0

    def get_previous_layer(self, network):
        if len(network.layers) > 0:
            return network.layers[-1]
        else:
            return None

    def line(self, neuron1, neuron2, weight):
        angle = arctan((neuron2.x - neuron1.x) / float(neuron2.y - neuron1.y))
        x_adjustment = neuron_radius * sin(angle)
        y_adjustment = neuron_radius * cos(angle)
        color = 'blue'
        if weight < 0:
            color = 'red'
        line = pyplot.Line2D((neuron1.x - x_adjustment, neuron2.x + x_adjustment),
                             (neuron1.y - y_adjustment, neuron2.y + y_adjustment),
                             linewidth=default_line_width * weight, color=color)  # HIER
        pyplot.gca().add_line(line)

    def draw(self):
        y = 0
        for neuron in self.neurons:
            if self.previous_layer:
                x = 0
                for previous_layer_neuron in self.previous_layer.neurons:
                    self.line(neuron, previous_layer_neuron, self.weights[x][y])
                    x += 1
            y += 1
            neuron.draw()


class NeuralNetwork():
    def __init__(self, architecture, weights):
        if not os.path.isdir('./models'):
            os.mkdir('./models')
        if not os.path.isdir('./models/NN_Images'):
            os.mkdir('./models/NN_Images')
        self.layers = []
        for i in range(len(architecture)):
            if i > 0:
                self.layers.append(Layer(self, architecture[i], weights[i - 1]))
            else:
                self.layers.append(Layer(self, architecture[i], None))

    def add_layer(self, number_of_neurons):
        layer = Layer(self, number_of_neurons)
        self.layers.append(layer)

    def draw(self, gen):
        for layer in self.layers:
            layer.draw()
        pyplot.axis('scaled')
        pyplot.savefig(f'./models/NN_Images/mlp_{gen}.png', dpi=300)
        pyplot.cla()
        #pyplot.show()


if __name__ == "__main__":
    network = NeuralNetwork([24, 12, 4])
    network.draw()
